FROM python:3.7

COPY backend/pyproject.toml .
RUN curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python && \
    PATH=~/.poetry/bin:$PATH && \
    poetry config virtualenvs.create false && \
    poetry install --no-dev

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.5.0/wait /wait
RUN chmod +x /wait
