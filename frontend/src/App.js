import Axios from "axios";
import React from "react";
import Select from "react-select";
import {
  Column,
  Table,
  Cell,
  ColumnHeaderCell,
  EditableCell
} from "@blueprintjs/table";
import { Drawer, Menu, MenuItem, Position } from "@blueprintjs/core";
import { Container, Row, Col } from "reactstrap";
import _ from "lodash";

const axios = Axios.create({ baseURL: "http://localhost:8000" });

class App extends React.Component {
  constructor(props) {
    super(props);

    this.countryOptions = [];
    this.indicatorOptions = [];
    this.countries = null;
    this.indicators = null;

    this.state = {
      data: {
        columns: [],
        indicators: {}
      },
      loading: true,
      error: null
    };

    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleIndicatorChange = this.handleIndicatorChange.bind(this);
    this.handleMenuClose = this.handleMenuClose.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.sortData = this.sortData.bind(this);
  }

  render() {
    const { data, error } = this.state;

    const errorDrawer = (
      <Drawer
        className="bg-warning"
        isOpen={error !== null}
        onClose={() => this.setState({ error: null })}
        position={Position.TOP}
        hasBackdrop={false}
        size="4%"
      >
        <h5 className="text-center">Can't connect to server</h5>
      </Drawer>
    );

    const placeholder = (
      <div
        className="d-flex justify-content-center align-items-center border rounded-lg mt-3"
        style={{ height: "80vh" }}
      >
        <h1 className="text-center" style={{ color: "grey" }}>
          Select Countries and Indicators
        </h1>
      </div>
    );

    const tables = Object.entries(data.indicators).map(
      ([indicator, countries]) => (
        <IndicatorTable
          key={indicator}
          indicator={indicator}
          data={countries}
          columns={data.columns}
          sortData={this.sortData}
          handleEdit={this.handleEdit}
        />
      )
    );

    return (
      <div>
        {errorDrawer}
        <Container>
          <Row>
            <Col xs="12" md="6" className="mt-3">
              <Select
                options={this.countryOptions}
                onChange={this.handleCountryChange}
                onMenuClose={this.handleMenuClose}
                closeMenuOnSelect={false}
                placeholder="Select Countries..."
                isMulti
              />
            </Col>
            <Col xs="12" md="6" className="mt-3">
              <Select
                options={this.indicatorOptions}
                onChange={this.handleIndicatorChange}
                onMenuClose={this.handleMenuClose}
                closeMenuOnSelect={false}
                placeholder="Select Indicators..."
                isMulti
              />
            </Col>
          </Row>
          {tables.length !== 0 ? tables : placeholder}
        </Container>
      </div>
    );
  }

  componentDidMount() {
    // Fetch countries and indicators to populate the select components

    Axios.all([axios.get("countries/"), axios.get("indicators/")])
      .then(
        Axios.spread((countries, indicators) => {
          this.countryOptions = countries.data.map(item => ({
            label: item.country_name,
            value: item.country_code
          }));
          this.indicatorOptions = indicators.data.map(item => ({
            label: item.indicator_name,
            value: item.indicator_code
          }));

          this.setState({ loading: false });
        })
      )
      .catch(error => {
        this.setState({ error: error });
      });
  }

  handleCountryChange(selections, { action }) {
    this.countries = _.isEmpty(selections)
      ? null
      : selections.map(item => item.value);
  }

  handleIndicatorChange(selections, { action }) {
    this.indicators = _.isEmpty(selections)
      ? null
      : selections.map(item => item.value);
  }

  handleMenuClose() {
    // Fetch data filtered by selected countries and indicators

    if (this.countries === null || this.indicators === null) {
      this.setState({ data: { columns: [], indicators: {} } });
      return;
    }

    axios
      .get("data/", {
        params: {
          countries: this.countries.join(","),
          indicators: this.indicators.join(",")
        }
      })
      .then(res => {
        this.setState({ data: reshapeData(res.data) });
      })
      .catch(error => {
        this.setState({ error: error });
      });
  }

  handleEdit(indicator, country, year, value, rowIndex, colIndex) {
    this.setState(state => {
      state.data.indicators[indicator][rowIndex][colIndex] = value;
      return state;
    });

    axios
      .patch("data/", {
        data: {
          indicator: indicator,
          country: country,
          year: `year_${year}`,
          value: value
        }
      })
      .catch(error => this.setState({ error: error }));
  }

  sortData(indicator, colIndex, direction) {
    const data = [...this.state.data.indicators[indicator]];
    const colIsStringType = data
      .map(item => typeof item[colIndex])
      .every(item => item === "string");

    data.sort((a, b) => {
      var valA = a[colIndex];
      var valB = b[colIndex];

      if (colIsStringType) {
        return direction === "asc"
          ? valA.localeCompare(valB)
          : valB.localeCompare(valA);
      }

      // Convert empty strings to -1 for comparison with numbers
      if (typeof valA === "string") {
        valA = -1;
      }

      if (typeof valB === "string") {
        valB = -1;
      }

      return direction === "asc" ? valA - valB : valB - valA;
    });

    this.setState(state => {
      state.data.indicators[indicator] = data;
      return state;
    });
  }
}

class IndicatorTable extends React.Component {
  constructor(props) {
    super(props);

    this.renderColumn = this.renderColumn.bind(this);
    this.renderCell = this.renderCell.bind(this);
    this.renderColumnHeaderCell = this.renderColumnHeaderCell.bind(this);
    this.renderMenu = this.renderMenu.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }

  render() {
    const numRows = this.props.data.length;
    const columns = this.props.columns.map((item, idx) =>
      this.renderColumn(item, idx)
    );

    return (
      <div className="mt-3">
        <h1>{this.props.indicator}</h1>
        <Table numRows={numRows} numFrozenColumns={1}>
          {columns}
        </Table>
      </div>
    );
  }

  renderColumn(colName, idx) {
    return (
      <Column
        key={idx}
        name={colName}
        cellRenderer={this.renderCell}
        columnHeaderCellRenderer={() =>
          this.renderColumnHeaderCell(colName, idx)
        }
      />
    );
  }

  renderCell(rowIndex, colIndex) {
    // Allow editing of values within the table

    const value = this.props.data[rowIndex][colIndex];

    if (colIndex === 0) {
      // Don't allow editing the country
      return <Cell>{value}</Cell>;
    }

    return (
      <EditableCell
        value={value}
        onConfirm={string => this.handleEdit(string, rowIndex, colIndex)}
        tooltip="Double-click to edit"
      />
    );
  }

  renderColumnHeaderCell(colName, idx) {
    return (
      <ColumnHeaderCell
        name={colName}
        menuRenderer={() => this.renderMenu(idx)}
      />
    );
  }

  renderMenu(idx) {
    const { sortData, indicator } = this.props;
    return (
      <Menu>
        <MenuItem
          onClick={() => sortData(indicator, idx, "asc")}
          text="Sort Asc"
        />
        <MenuItem
          onClick={() => sortData(indicator, idx, "desc")}
          text="Sort Desc"
        />
      </Menu>
    );
  }

  handleEdit(string, rowIndex, colIndex) {
    const { indicator, data, columns } = this.props;

    const year = columns[colIndex];
    const country = data[rowIndex][0];

    return this.props.handleEdit(
      indicator,
      country,
      year,
      string,
      rowIndex,
      colIndex
    );
  }
}

function reshapeData(data) {
  // Reshape data
  //  From: [{country: 1, indicator: 1, year_1960: 1286, ...}, ...]
  //  To: {columns: [Country, 1960, ...], data: { indicator_1: [Andorra, 1676, 3795, ...], ...}}

  const new_data = {
    columns: [],
    indicators: {}
  };

  Object.keys(data[0]).forEach(item => {
    if (item.startsWith("year")) {
      new_data.columns.push(item.split("_")[1]);
    } else if (item === "country") {
      new_data.columns.push("Country");
    }
  });

  data.forEach(item => {
    const { indicator } = item;

    delete item.indicator;

    if (!Object.keys(new_data.indicators).includes(indicator.toString())) {
      new_data.indicators[indicator] = [];
    }

    new_data.indicators[indicator].push(
      Object.values(item).map(val => parseVal(val))
    );
  });

  return new_data;
}

function parseVal(val) {
  // Parse as float or int

  if (/^[0-9]+\.[0-9]+$/.test(val)) {
    return parseFloat(val);
  }

  if (/^[0-9]+$/.test(val)) {
    return parseInt(val);
  }

  return val;
}

export default App;
