# Running Locally

## Requirements

- Docker
- Docker Compose
- npm

## Steps

```
# Start Postgres and Django
cd data-tables
docker-compose up -d

# Start React
cd frontend
npm install
npm start
```

Open a browser to http://localhost:3000
