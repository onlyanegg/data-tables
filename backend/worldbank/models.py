from django.db import models

from worldbank import settings


# TODO: Maybe region and income group should be categorial
class Country(models.Model):
    country_name = models.CharField(max_length=100)
    country_code = models.CharField(max_length=10, unique=True)
    region = models.CharField(max_length=100)
    income_group = models.CharField(max_length=100)
    special_notes = models.CharField(max_length=2000)


class Indicator(models.Model):
    indicator_name = models.CharField(max_length=200)
    indicator_code = models.CharField(max_length=20, unique=True)
    source_note = models.CharField(max_length=2000)
    source_organization = models.CharField(max_length=2000)


class Data(models.Model):
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    indicator = models.ForeignKey(Indicator, on_delete=models.CASCADE)

    ordering = ["country"]


# Add year fields to the Data table
for year in range(settings.START_YEAR, settings.END_YEAR + 1):
    models.CharField(max_length=50).contribute_to_class(Data, f"year_{year}")
