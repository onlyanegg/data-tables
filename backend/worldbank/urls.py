from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from worldbank.views import CountryView, DataView, IndicatorView

urlpatterns = [
    path("data/", csrf_exempt(DataView.as_view())),
    path("countries/", csrf_exempt(CountryView.as_view())),
    path("indicators/", csrf_exempt(IndicatorView.as_view())),
]
