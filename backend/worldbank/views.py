from rest_framework import generics, mixins
from rest_framework.response import Response
from rest_framework.status import (HTTP_200_OK, HTTP_400_BAD_REQUEST,
                                   HTTP_404_NOT_FOUND)

from worldbank import settings
from worldbank.models import Country, Data, Indicator
from worldbank.serializers import (CountrySerializer, DataSerializer,
                                   IndicatorSerializer)


class CountryView(generics.GenericAPIView, mixins.ListModelMixin):
    serializer_class = CountrySerializer
    pagination_class = None

    def get(self, request, *args, **kwargs):
        """
        """

        return self.list(request, *args, **kwargs)

    def get_queryset(self):
        """
        """

        return Country.objects.all()


class IndicatorView(generics.GenericAPIView, mixins.ListModelMixin):
    serializer_class = IndicatorSerializer
    pagination_class = None

    def get(self, request, *args, **kwargs):
        """
        """

        return self.list(request, *args, **kwargs)

    def get_queryset(self):
        """
        """

        return Indicator.objects.all()


class DataView(generics.GenericAPIView, mixins.ListModelMixin):
    serializer_class = DataSerializer
    ordering_fields = ("country", "indicator") + tuple(
        f"year_{y}" for y in range(settings.START_YEAR, settings.END_YEAR + 1)
    )
    pagination_class = None

    def get(self, request, *args, **kwargs):
        """
        """

        return self.list(request, *args, **kwargs)

    def patch(self, request, format=None):
        """
        """

        try:
            indicator = request.data["data"]["indicator"]
            country = request.data["data"]["country"]
            year = request.data["data"]["year"]
            value = request.data["data"]["value"]
        except KeyError:
            return Response(
                f"Indicator, Country, and Year are required. Sent: {request.data}",
                status=HTTP_400_BAD_REQUEST,
            )

        queryset = Data.objects.all()
        queryset = queryset.filter(indicator__indicator_name__exact=indicator)
        queryset = queryset.filter(country__country_name__exact=country)

        data = queryset.first()
        if data == None:
            return Response(status=HTTP_404_NOT_FOUND)

        setattr(data, year, value)
        data.save(update_fields=[year])

        return Response(status=HTTP_200_OK)

    def get_queryset(self):
        """
        """

        queryset = Data.objects.all()

        if "countries" in self.request.query_params:
            country_codes = [
                c.strip() for c in self.request.query_params["countries"].split(",")
            ]
            queryset = queryset.filter(country__country_code__in=country_codes)

        if "indicators" in self.request.query_params:
            indicator_codes = [
                i.strip() for i in self.request.query_params["indicators"].split(",")
            ]
            queryset = queryset.filter(indicator__indicator_code__in=indicator_codes)

        return queryset
