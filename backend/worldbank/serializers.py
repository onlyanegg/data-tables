from rest_framework.serializers import ModelSerializer, SlugRelatedField

from worldbank import settings
from worldbank.models import Country, Data, Indicator


class CountrySerializer(ModelSerializer):
    class Meta:
        model = Country
        fields = ("country_name", "country_code")


class IndicatorSerializer(ModelSerializer):
    class Meta:
        model = Indicator
        fields = ("indicator_name", "indicator_code")


class DataSerializer(ModelSerializer):
    country = SlugRelatedField(slug_field="country_name", read_only=True)
    indicator = SlugRelatedField(slug_field="indicator_name", read_only=True)

    class Meta:
        model = Data
        fields = ("country", "indicator") + tuple(
            f"year_{y}" for y in range(settings.START_YEAR, settings.END_YEAR + 1)
        )
