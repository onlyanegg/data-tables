import csv
import os
import re
from os.path import join

from django.core.management.base import BaseCommand
from slugify import slugify

from worldbank.models import Country, Data, Indicator

# Build map from country codes to country names for the countries table as it is not in the country
# metadata files
country_map = {}
with open("./worldbank/data/country_map") as f:
    for row in f:
        split = row.split()
        code = split[0]
        name = split[1:]
        country_map[code] = " ".join(name)


class Command(BaseCommand):
    data_dir = "./worldbank/data"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def add_arguments(self, parser):
        parser.add_argument(
            "-d",
            "--data",
            type=str,
            nargs="*",
            help="Type of data to insert",
            choices=["countries", "indicators", "data",],
        )

    def handle(self, *args, **options):
        """
        Save initial data to the database
        """

        self.stdout.write("Inserting data")
        if options.get("data"):
            if "countries" in options["data"]:
                self._insert_countries()
            if "indicators" in options["data"]:
                self._insert_indicators()
            if "data" in options["data"]:
                self._insert_data()
        self.stdout.write("Finished inserting data")

    def _insert_countries(self):
        """
        """

        if Country.objects.count() > 0:
            self.stdout.write(
                "Countries have already been inserted. Remove them manually or rebuild the database to reinsert."
            )
            return

        with open(
            join(
                self.data_dir,
                "Metadata_Country_API_EN.ATM.CO2E.PC_DS2_en_csv_v2_713061.csv",
            )
        ) as f:
            reader = csv.DictReader(f)
            for row in reader:
                row = keys_to_snake_case(row)
                row["country_name"] = country_map[row["country_code"]]
                del row["table_name"]
                del row[""]
                country = Country(**row)
                country.save()

        # This one isn't included
        country = Country(country_code="INX", country_name=country_map["INX"])
        country.save()

    def _insert_indicators(self):
        """
        """

        if Indicator.objects.count() > 0:
            self.stdout.write(
                "Indicators have already been inserted. Remove them manually or rebuild the database to reinsert."
            )
            return

        for file_ in [
            join(self.data_dir, f)
            for f in os.listdir(self.data_dir)
            if "Metadata_Indicator" in f
        ]:
            with open(file_) as f:
                reader = csv.DictReader(f)
                for row in reader:
                    row = keys_to_snake_case(row)
                    del row[""]
                    indicator = Indicator(**row)
                    indicator.save()

    def _insert_data(self):
        """
        """

        if Data.objects.count() > 0:
            self.stdout.write(
                "Data has already been inserted. Remove them manually or rebuild the database to reinsert."
            )
            return

        year_pattern = re.compile(r"[0-9]{4}")
        for file_ in [
            join(self.data_dir, f)
            for f in os.listdir(self.data_dir)
            if f.startswith("API_")
        ]:
            with open(file_) as f:
                reader = csv.DictReader(f)
                for row in reader:
                    row = keys_to_snake_case(row)

                    # Get Foreign Keys
                    try:
                        country = Country.objects.get(country_code=row["country_code"])
                        indicator = Indicator.objects.get(
                            indicator_code=row["indicator_code"]
                        )
                    except:
                        breakpoint()

                    # Clean data
                    row["country"] = country
                    row["indicator"] = indicator
                    del row["country_name"]
                    del row["country_code"]
                    del row["indicator_name"]
                    del row["indicator_code"]
                    del row[""]

                    # Covert eg. '1999' to 'year_1999'
                    keys = list(row.keys())
                    for key in keys:
                        match = re.match(year_pattern, key)
                        if match:
                            year = match[0]
                            row[f"year_{year}"] = row[year]
                            del row[year]

                    # Save
                    datum = Data(**row)
                    datum.save()


def keys_to_snake_case(dict_):
    new_dict = {}
    for key in dict_.keys():
        new_key = re.sub(r"([a-z])([A-Z])", r"\1 \2", key)
        new_key = slugify(new_key, separator="_")
        new_dict[new_key] = dict_[key]

    return new_dict


def country_code_to_name(code):
    """
    """
