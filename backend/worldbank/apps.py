from django.apps import AppConfig


class WorldbankConfig(AppConfig):
    name = 'worldbank'
